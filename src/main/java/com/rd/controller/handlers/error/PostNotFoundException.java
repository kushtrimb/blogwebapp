package com.rd.controller.handlers.error;

/**
 * Created by Rinor Maloku.
 */
public class PostNotFoundException extends RuntimeException
{
    private long postId;

    public PostNotFoundException(long postId)
    {
        this.postId = postId;
    }

    public long getPostId()
    {
        return postId;
    }
}
