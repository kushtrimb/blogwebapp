package com.rd.controller.handlers.error;

/**
 * Created by Rinor Maloku.
 */
public class ApiError
{
    private long code;
    private String message;

    public ApiError(long code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public long getCode()
    {
        return code;
    }

    public String getMessage()
    {
        return message;
    }
}
