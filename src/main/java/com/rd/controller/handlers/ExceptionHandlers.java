package com.rd.controller.handlers;

import com.rd.controller.handlers.error.ApiError;
import com.rd.controller.handlers.error.PostNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Rinor Maloku.
 */
@ControllerAdvice
public class ExceptionHandlers
{
    @ExceptionHandler(PostNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ApiError> postNotFound(PostNotFoundException e)
    {
        final long postId = e.getPostId();
        return new ResponseEntity<>(
                new ApiError(postId, String.format("Post with id: %d not found", postId)),
                HttpStatus.NOT_FOUND);
    }
}
