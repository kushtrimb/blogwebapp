package com.rd.controller;

import com.rd.controller.handlers.error.PostNotFoundException;
import com.rd.dto.CommentDto;
import com.rd.dto.NewCommentDto;
import com.rd.dto.PostDto;
import com.rd.services.CommentServices;
import com.rd.services.PostServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/post")
public class PostController
{
    @Autowired
    private CommentServices commentService;

    @Autowired
    private PostServices postService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PostDto comment(@PathVariable Long id)
    {
        return postService.getPost(id);
    }

    @RequestMapping(value = "/{id}/comment", method = RequestMethod.POST, consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CommentDto> saveComment(@PathVariable Long id, @RequestBody NewCommentDto comment)
            throws PostNotFoundException
    {
        final PostDto post = postService.getPost(id);

        if (post == null) throw new PostNotFoundException(id);

        comment.setPostId(id);
        final long genId = commentService.addComment(comment);
        final CommentDto comment1 = commentService.getComment(genId);
        return new ResponseEntity<>(comment1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}/comments", method = RequestMethod.GET)
    public List<CommentDto> getComments(@PathVariable Long id)
    {
        return commentService.getCommentsForPost(id);
    }

    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public List<CommentDto> getAllComments()
    {
        return commentService.findAllComments();
    }
}
