package com.rd.services;

import com.rd.domain.Post;
import com.rd.dto.PostDto;
import com.rd.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Kushtrim.Bytyqi on 6/23/2017.
 */
@Service
@Transactional
public class PostServices{

    @Autowired
    private PostRepository postRepository;

    public PostDto getPost(Long id) {
        Post post = postRepository.findOne(id);
        if (post != null) {
            return new PostDto(post.getTitle(), post.getContent(), post.getCreationDate());
        } else {
            return null;
        }
    }
}
