package com.rd.services;

import com.rd.domain.Comment;
import com.rd.domain.Post;
import com.rd.dto.CommentDto;
import com.rd.dto.NewCommentDto;
import com.rd.repository.CommentRepository;
import com.rd.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Kushtrim.Bytyqi on 6/23/2017.
 */
@Service
@Transactional
public class CommentServices
{
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    PostRepository postRepository;

    public CommentDto getComment(Long id)
    {
        Comment comment = commentRepository.findOne(id);
        if (comment != null)
        {
            return new CommentDto(comment.getId(), comment.getComment(),
                    comment.getAuthor(), comment.getCreationDate());
        } else
        {
            return null;
        }
    }

    public List<CommentDto> findAllComments(){
        Iterable<Comment> comments =  commentRepository.findAll();
        List<CommentDto> comms = new ArrayList();
        for(Comment comment : comments){
            comms.add(new CommentDto(comment.getId(), comment.getComment(),
                    comment.getAuthor(), comment.getCreationDate()));
        }
        return comms;
    }

    public List<CommentDto> getCommentsForPost(Long postId)
    {
        List<CommentDto> clist = new ArrayList<>();
        Comment com = null;
        CommentDto ct = null;
        List<Comment> cslist =  postRepository.findOne(postId).getComments();

        Iterable<Post> posts = postRepository.findAll();

        Iterable<Comment> coments = commentRepository.findAll();

        System.out.println("==>");
        posts.forEach(post -> System.out.println(post));
        System.out.println("==>");
        System.out.println("==>");
        coments.forEach(post -> System.out.println(coments));
        System.out.println("==>");



        for(int i=0;i<cslist.size();i++){
            com = cslist.get(i);
            ct = new CommentDto(com.getId(), com.getComment(),
                    com.getAuthor(), com.getCreationDate());
        }
        return clist;
    }

    public Long addComment(NewCommentDto newCommentDto)
    {
        Post post = postRepository.findOne(newCommentDto.getPostId());
        Comment comment = new Comment(newCommentDto.getAuthor(), newCommentDto.getContent(), new Date(), post);
        post.setComment(comment);
        return commentRepository.save(comment).getId();
    }

}
